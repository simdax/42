/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   match.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/12 13:38:06 by scornaz           #+#    #+#             */
/*   Updated: 2017/08/13 15:53:28 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		match(char *s1, char *s2)
{
	if (!*s1 && !*s2)
		return (1);
	if (*s1 == *s2)
		return (match(++s1, ++s2));
	if (*s1 && *s2 == '*')
		return (match(s1, ++s2) || match(s1++, s2));
	if (!*s1 && *s2 == '*')
		return (match(s1, ++s2));
	return (0);
}

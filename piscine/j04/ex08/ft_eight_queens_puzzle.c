/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 10:55:14 by scornaz           #+#    #+#             */
/*   Updated: 2017/08/08 16:21:19 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	update_mat(int tab[8][8], int x, int y, int increment)
{
	int i;

	i = 0;
	while (i < 8)
		tab[x][i++] += increment;
	i = 0;
	while (i < 8)
		tab[i++][y] += increment;
	tab[x][y] -= increment;
	i = 0;
	while (x + ++i < 8 && y + i < 8)
		tab[x + i][y + i] += increment;
	i = 0;
	while (x - ++i >= 0 && y + i < 8)
		tab[x - i][y + i] += increment;
	i = 0;
	while (x - ++i >= 0 && y - i >= 0)
		tab[x - i][y - i] += increment;
	i = 0;
	while (x + ++i < 8 && y - i >= 0)
		tab[x + i][y - i] += increment;
}

int		put_queen(int tab[8][8], int col, int *solutions)
{
	int row;

	row = 0;
	while (row < 8)
	{
		if (tab[row][col] == 0)
		{
			if (col == 7)
			{
				(*solutions)++;
			}
			else
			{
				update_mat(tab, row, col, 1);
				if (!put_queen(tab, col + 1, solutions))
					update_mat(tab, row, col, -1);
			}
		}
		row++;
	}
	return (0);
}

int		ft_eight_queens_puzzle(void)
{
	int solutions;
	int i;
	int j;
	int tab[8][8];

	solutions = 0;
	i = 0;
	while (i < 8)
	{
		j = 0;
		while (j < 8)
		{
			tab[i][j] = 0;
			j++;
		}
		i++;
	}
	put_queen(tab, 0, &solutions);
	return (solutions);
}

/* ft_eight_queens_puzzle.c */
/* ft_eight_queens_puzzle_2.c */
/* ft_fibonacci */
/* ft_find_next_prime.c */
/* ft_is_prime.c */
//ft_iterative_power.c

#include "headers.h"

#include <stdio.h>
#include <time.h>

//void benchmark()
//{}

int main () 
{
/* 	printf("%d\n", ft_iterative_factorial(16)); */
/* 	printf("%d\n", ft_recursive_factorial(20)); */
/* 	int i; */
/* 	for(i = 0; i < 3; i++)	 */
/* 	{ */
/* 		printf("%d\n", ft_recursive_power(3, i));	 */
/* 		printf("%d\n", ft_iterative_power(3, i)); */
/* 	} */
/* 	printf("sqrt of %d=\n", ft_sqrt(2147483647)); */
/* 	printf("sqrt = %d\n", ft_sqrt(30858025)); */

/* 	for(i = 0; i < 100; i++)	 */
/* 	{ */
/* 		printf("%d is prime ? %d\n", i, ft_is_prime(i)); */
/*  	}  */
/* 	float startTime = (float)clock()/CLOCKS_PER_SEC; */
/* 	printf("max %d\n", ft_is_prime(2147483647)); */
/* 	float endTime = (float)clock()/CLOCKS_PER_SEC; */
/* 	float timeElapsed = endTime - startTime; */
/* 	printf("time elapsed %f\n", timeElapsed); */
	
/* 	printf("next of %d %d \n", 	ft_find_next_prime(480000), 480000); */
/* 	printf("next of %d %d \n", 	ft_find_next_prime(48000), 48000); */
/* 	printf("next of %d %d \n", 	ft_find_next_prime(4800), 4800); */

	ft_eight_queens_puzzle_2();

}

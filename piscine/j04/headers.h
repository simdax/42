
#include "./ex00/ft_iterative_factorial.c"
#include "./ex01/ft_recursive_factorial.c" 
#include "./ex02/ft_iterative_power.c" 
#include "./ex03/ft_recursive_power.c" 
#include "./ex04/ft_fibonacci.c" 
#include "./ex05/ft_sqrt.c" 
#include "./ex06/ft_is_prime.c"
#include "./ex07/ft_find_next_prime.c"
#include "./ex08/ft_eight_queens_puzzle.c" 
#include "./ex09/ft_eight_queens_puzzle_2.c"

int ft_iterative_factorial(int nb);
int ft_recursive_factorial(int nb);
int ft_iterative_power(int nb, int pow);
int ft_recursive_power(int nb, int pow);
int ft_fibonacci(int nb);
int ft_sqrt(int nb);
int ft_is_prime(int nb);
int ft_find_next_prime(int nb);
int ft_eight_queens_puzzle(void);
void ft_eight_queens_puzzle_2(void);

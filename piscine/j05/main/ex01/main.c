void ft_putnbr(int);

#include <limits.h>

int main()
{
	ft_putnbr(42);
	ft_putnbr(-42);
	ft_putnbr(INT_MAX);
	ft_putnbr(INT_MIN);
	return (0);
}

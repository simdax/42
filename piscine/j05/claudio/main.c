/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 16:23:58 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/16 11:54:51 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_test_day_05(int from, int to);
void	ft_test_day_07(int from, int to);
void	ft_test_day_10(int from, int to);

int main ()
{

		ft_test_day_05(0,20);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 10:21:35 by scornaz           #+#    #+#             */
/*   Updated: 2017/08/15 11:38:36 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int	ft_add(int i, int j)
{
	return (i + j);
}

int	ft_sub(int i, int j)
{
	return (i - j);
}

int	ft_mul(int i, int j)
{
	return (i * j);
}

int	ft_div(int i, int j)
{
	return (i / j);
}

int	ft_mod(int i, int j)
{
	return (i % j);
}

    void	add(void *a, void *b);
    void	del(void *data, size_t size);
    t_list	*hydrate(char **chaine);
    int main(int argc, char **argv);
    void	push(t_list *l_a, t_list *l_b, int val);
    void	rotate(t_list *l);
    void	swap(t_list *elem);
